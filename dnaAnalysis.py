import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)

# A = 'gatcacaggtctatcaccctattaaccactcacgggagctctccatgcatttggtatttt'
# B = 'ggcataacacagcaagacgagaagaccctatggagctttaatttattaatgcaaacagta'
dp = []
dictionary = {
    'aa': 5,
    'ac': -1,
    'ag': -2,
    'at': -1,
    'a-': -3,
    'ca': -1,
    'cc': 5,
    'cg': -3,
    'ct': -2,
    'c-': -4,
    'ga': -2,
    'gc': -3,
    'gg': 5,
    'gt': -2,
    'g-': -2,
    'ta': -1,
    'tc': -2,
    'tg': -2,
    'tt': 5,
    't-': -1,
    '-a': -3,
    '-c': -4,
    '-g': -2,
    '-t': -1
}


def dna_alignment(A, B, a, b, cache):
    print(a, b)
    cache[0][0] = 0
    for i in range(1, b + 1):
        print(i)
        index = B[i].__str__() + '-'
        cache[0][i] = cache[0][i - 1] + dictionary[index]
    for j in range(1, a + 1):
        index = A[j].__str__() + '-'
        cache[j][0] = cache[j - 1][0] + dictionary[index]
    for i in range(1, a + 1):
        for j in range(1, b + 1):
            print(i, j)
            index_sub = A[i].__str__() + B[j].__str__()
            index_delA = A[i].__str__() + '-'
            index_delB = B[j].__str__() + '-'
            cache[i][j] = max(dictionary[index_delA] + cache[i - 1][j], dictionary[index_delB] + cache[i][j - 1], cache[i - 1][j - 1] + dictionary[index_sub])
    return cache[a][b]


def traceback(A, B, a, b, cache):
    list = []
    while True:
        if a == 0 and b == 0:
            string = A[a].__str__() + B[b].__str__()
            list.append(string)
            return list
        if a == 0:
            for i in range(b, 0, -1):
                string = B[i].__str__() + '-'
                list.append(string)
                a = 0
            continue
        if b == 0:
            for i in range(a, 0, -1):
                string = A[i].__str__() + '-'
                list.append(string)
                a = 0
            continue

        swap = cache[a - 1][b - 1]
        delete = cache[a - 1][b]
        insert = cache[a][b - 1]

        if swap > delete:
            if swap > insert:
                max = 'swap'
            else:
                max = 'insert'
        elif delete > insert:
            max = 'delete'
        else:
            max = 'insert'

        if max == 'swap':
            string = A[a].__str__() + B[b].__str__()
            list.append(string)
            a -= 1
            b -= 1
            continue
        elif max == 'insert':
            string = B[b].__str__() + '-'
            list.append(string)
            b -= 1
            continue
        else:
            string = A[a].__str__() + '-'
            list.append(string)
            a -= 1
            continue


def humans():
    files = []
    american = open("input_data/american.txt", "r")
    files.append(american)
    australian = open("input_data/australianaborigine.txt", "r")
    files.append(australian)
    china = open("input_data/china.txt", "r")
    files.append(china)
    dutch = open("input_data/dutch.txt", "r")
    files.append(dutch)
    ethiopia = open("input_data/ethiopia.txt", "r")
    files.append(ethiopia)
    greece = open("input_data/greece.txt", "r")
    files.append(greece)
    india = open("input_data/india.txt", "r")
    files.append(india)
    lebanon = open("input_data/lebanon.txt", "r")
    files.append(lebanon)
    navajo = open("input_data/navajo.txt", "r")
    files.append(navajo)
    tonga = open("input_data/tonga.txt", "r")
    files.append(tonga)

    output = open("output_data/humans_output.txt", "a+")

    for i in range(len(files)):
        file1 = files[i]
        sequence1 = ''
        for line in file1:
            string = line.split(' ')
            for n in range(0, len(string)):
                seq = string[n].split("\n")
                sequence1 += seq[0]
        print(sequence1)
        for j in range(i + 1, len(files)):
            sequence2 = ''
            file2 = files[j]
            for line in file2:
                string = line.split(' ')
                for n in range(0, len(string)):
                    seq = string[n].split("\n")
                    sequence2 += seq[0]
            cache = [[0 for l in range(0, len(sequence2))] for k in range(0, len(sequence1))]
            result = dna_alignment(sequence1, sequence2, len(sequence1) - 1, len(sequence2) - 1, cache)
            output.write(str(i + 1)+" "+str(j + 1)+"Result: "+str(result)+"\n")


if __name__ == '__main__':
    humans()
    # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # dna_alignment(len(A) - 1, len(B) - 1)
    # list = []
    # list = traceback(len(A) - 1, len(B) - 1, list)
    # for i in range(0, len(list)):
    #     print(list[i])
    # human = open("human.txt", "r")
    # neandertal = open("neandertal.txt", "r")
    # human_sequence = ''
    # neandertal_sequence = ''
    #
    # for line in human:
    #     string = line.split(' ')
    #     for i in range(1, len(string)):
    #         seq = string[i].split("\n")
    #         human_sequence += seq[0]
    #
    # for line in neandertal:
    #     string = line.split(' ')
    #     for i in range(1, len(string)):
    #         seq = string[i].split("\n")
    #         neandertal_sequence += seq[0]
    # A = human_sequence
    # B = neandertal_sequence
    # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # dna_alignment(len(A) - 1, len(B) - 1)
    # print(dp)
    # temp = open("temp.txt", "w")
    # temp.write(dp.__str__())
    # list = []
    # list = traceback(len(A) - 1, len(B) - 1, list)
    # results_file = open("human_neandertal.txt", "w")
    # for i in range(0, len(list)):
    #     results_file.write(list[i])
    # results_file.close()
    # temp.close()
    # human.close()
    # neandertal.close()

    # american = open("american.txt", "r")
    # australian = open("australianaborigine.txt", "r")
    # china = open("china.txt", "r")
    # dutch = open("dutch.txt", "r")
    # ethiopia = open("ethiopia.txt", "r")
    # greece = open("greece.txt", "r")
    # india = open("india.txt", "r")
    # lebanon = open("lebanon.txt", "r")
    # navajo = open("navajo.txt", "r")
    # tonga = open("tonga.txt", "r")
    #
    # american_sequence = ''
    # australian_sequence = ''
    # china_sequence = ''
    # dutch_sequence = ''
    # ethiopia_sequence = ''
    # greece_sequence = ''
    # india_sequence = ''
    # lebanon_sequence = ''
    # navajo_sequence = ''
    # tonga_sequence = ''
    #
    # for line in american:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         american_sequence += seq[0]
    # print(american_sequence)
    #
    # for line in australian:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         australian_sequence += seq[0]
    # print(australian_sequence)
    #
    # for line in china:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         china_sequence += seq[0]
    # print(china_sequence)
    #
    # for line in dutch:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         dutch_sequence += seq[0]
    # print(dutch_sequence)
    #
    # for line in ethiopia:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         ethiopia_sequence += seq[0]
    # print(ethiopia_sequence)
    #
    # for line in greece:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         greece_sequence += seq[0]
    # print(greece_sequence)
    #
    # for line in india:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         india_sequence += seq[0]
    # print(india_sequence)
    #
    # for line in lebanon:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         lebanon_sequence += seq[0]
    # print(lebanon_sequence)
    #
    # for line in navajo:
    #     string = line.split(' ')
    #     for i in range(1, len(string)):
    #         seq = string[i].split("\n")
    #         navajo_sequence += seq[0]
    # print(navajo_sequence)
    #
    # for line in tonga:
    #     string = line.split(' ')
    #     for i in range(0, len(string)):
    #         seq = string[i].split("\n")
    #         tonga_sequence += seq[0]
    # print(tonga_sequence)
    #
    # results_file = open("distances.txt", "w")
    # A = american_sequence
    # # B = australian_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Australian: ' + result.__str__() + '\n')
    #
    # # B = china_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->China: ' + result.__str__() + '\n')
    #
    # # B = dutch_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Dutch: ' + result.__str__() + '\n')
    #
    # B = ethiopia_sequence
    # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # result = dna_alignment(len(A) - 1, len(B) - 1)
    # results_file.write('American->Ethiopia: ' + result.__str__() + '\n')
    # #
    # # B = greece_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Greece: ' + result.__str__() + '\n')
    # #
    # # B = india_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->India: ' + result.__str__() + '\n')
    # #
    # # B = lebanon_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Lebanon: ' + result.__str__() + '\n')
    # #
    # # B = navajo_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Navajo: ' + result.__str__() + '\n')
    # #
    # # B = tonga_sequence
    # # dp = np.array([[0 for i in range(0, len(B))] for j in range(0, len(A))])
    # # result = dna_alignment(len(A) - 1, len(B) - 1)
    # # results_file.write('American->Tonga: ' + result.__str__() + '\n')
    #
    # results_file.close()
    # american.close()
    # australian.close()
    # china.close()
    # dutch.close()
    # ethiopia.close()
    # greece.close()
    # india.close()
    # lebanon.close()
    # navajo.close()
    # tonga.close()
    #
    # # file = open("human_neandertal.txt", "r")
    # # file2 = open("human_neandertal_results.txt", "w")
    # # counter = 1
    # # for line in file:
    # #     arr = [line[i:i+2] for i in range(0, len(line), 2)]
    # #     for j in arr:
    # #         file2.write(j + '\n')
    # # file.close()
    # # file2.close()
